<?php

namespace Mageplaza\HelloWorld\Block;

class Hometask extends \Magento\Framework\View\Element\Template
{
    protected $helperData;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Mageplaza\HelloWorld\Helper\Data                $helperData)
    {
        $this->helperData = $helperData;
        parent::__construct($context);
    }


    public function getTestValue1()
    {
        return $this->helperData->getGeneralConfig('test1_text');


    }

    public function getTestValue2()
    {
        return $this->helperData->getGeneralConfig('test2_text');


    }

    public function getTestValue3()
    {
        return $this->helperData->getGeneralConfig('test3_text');

    }
}


